#!/bin/bash

#. env.sh

BUILD_DEBUG=1

BUILD_TYPE=Release

CMAKE=cmake

PARAMS="${PARAMS} -D CMAKE_INSTALL_PREFIX=../install"
#PARAMS="${PARAMS} -D MZLUTILS_PATH=../../mzlUtils/Install"

#source libsetup enable lua
#PARAMS="${PARAMS} -D LUA_DIR:PATH=${LUA_BASE_DIR}"
    
set -e

mkdir -p Build${BUILD_TYPE}
cd Build${BUILD_TYPE}
${CMAKE} -D CMAKE_BUILD_TYPE=${BUILD_TYPE} ${PARAMS} ..
make -j8
make install
cd ..

if [ "$BUILD_DEBUG" = "1" ]; then
    BUILD_TYPE=Debug
    
    mkdir -p Build${BUILD_TYPE}
    cd Build${BUILD_TYPE}
    ${CMAKE} -D CMAKE_BUILD_TYPE=${BUILD_TYPE} ${PARAMS} ..
    make -j8
    make install
    cd ..
fi

