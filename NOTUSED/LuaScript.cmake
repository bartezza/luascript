#====================================================================================================#
#  ScialloSettings
#====================================================================================================#
#
# inputs: SCIALLOSETTINGS_PATH (install path)
# outputs: SCIALLOSETTINGS_LIB (the single library), SCIALLOSETTINGS_LIB_STATIC (static lib),
#          SCIALLOSETTINGS_LIBS (library + deps), SCIALLOSETTINGS_LIBS_STATIC, SCIALLOSETTINGS_DEP_LIBS(only deps)
#
# version: v2.0
#

#if ( NOT SCIALLOSETTINGS_DEFAULT_PATH )
#    set( SCIALLOSETTINGS_DEFAULT_PATH "${PROJECT_SOURCE_DIR}/../mzlUtils" )
#endif ()

# mzlUtils path + build path
#set( SCIALLOSETTINGS_PATH ${SCIALLOSETTINGS_DEFAULT_PATH} CACHE PATH "" )

find_library( SCIALLOSETTINGS_LIB_RELEASE ScialloSettings PATHS ${SCIALLOSETTINGS_PATH} PATH_SUFFIXES "lib" )
find_library( SCIALLOSETTINGS_LIB_RELEASE_STATIC ScialloSettings_static PATHS ${SCIALLOSETTINGS_PATH} PATH_SUFFIXES "lib" STATIC )
find_library( SCIALLOSETTINGS_LIB_DEBUG ScialloSettings_debug PATHS ${SCIALLOSETTINGS_PATH} PATH_SUFFIXES "lib" )
find_library( SCIALLOSETTINGS_LIB_DEBUG_STATIC ScialloSettings_debug_static PATHS ${SCIALLOSETTINGS_PATH} PATH_SUFFIXES "lib" STATIC )

if ( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
    if ( SCIALLOSETTINGS_LIB_DEBUG )
        set( SCIALLOSETTINGS_LIB ${SCIALLOSETTINGS_LIB_DEBUG} )
    else ()
        message( "[WARNING] ScialloSettings debug lib not found!" )
    endif ()
    if ( SCIALLOSETTINGS_LIB_DEBUG_STATIC )
        set( SCIALLOSETTINGS_LIB_STATIC ${SCIALLOSETTINGS_LIB_DEBUG_STATIC} )
    else ()
        message( "[WARNING] ScialloSettings static debug lib not found!" )
    endif ()
else ()
    if ( SCIALLOSETTINGS_LIB_RELEASE )
        set( SCIALLOSETTINGS_LIB ${SCIALLOSETTINGS_LIB_RELEASE} )
    else ()
        message( "[WARNING] ScialloSettings release lib not found!" )
    endif ()
    if ( SCIALLOSETTINGS_LIB_RELEASE_STATIC )
        set( SCIALLOSETTINGS_LIB_STATIC ${SCIALLOSETTINGS_LIB_RELEASE_STATIC} )
    else ()
        message( "[WARNING] ScialloSettings static release lib not found!" )
    endif ()
endif ()

include_directories( ${SCIALLOSETTINGS_PATH}/include )

#get_filename_component( SCIALLOSETTINGS_LIBRARY_PATH ${SCIALLOSETTINGS_LIB} PATH )
#include_directories( ${SCIALLOSETTINGS_LIBRARY_PATH} )

#if ( NOT WIN32 )
#    add_definitions( "-DLINUX" )
#endif ()

message( "-- ScialloSettings library at ${SCIALLOSETTINGS_LIB}, static at ${SCIALLOSETTINGS_LIB_STATIC}" )

# dependencies
#include( "${SCIALLOSETTINGS_PATH}/mzlUtils_deps.cmake" )
set( SCIALLOSETTINGS_LIBS ${SCIALLOSETTINGS_LIB} ${SCIALLOSETTINGS_DEP_LIBS} )
set( SCIALLOSETTINGS_LIBS_STATIC ${SCIALLOSETTINGS_LIB_STATIC} ${SCIALLOSETTINGS_DEP_LIBS} )

message( "-- SCIALLOSETTINGS_LIBS = ${SCIALLOSETTINGS_LIB}" )

