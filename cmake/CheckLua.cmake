# ===== lua =====
#
# input: LUA_DIR, [LUA_INCLUDE_DIR], [LUA_LINK_DIR], LIBRARIES_DIR, DEP_LIBS
# output: LUA_LIBRARY, USE_LUA, DEP_LIBS
#

if ( NOT LUA_DIR )
    if ( LIBRARIES_DIR )
        set( LUA_DIR ${LIBRARIES_DIR} CACHE PATH "lua base dir" )
    else ()
        set( LUA_DIR "${PROJECT_SOURCE_DIR}/external/lua53" CACHE PATH "lua base dir" )
    endif ()
endif ()
set( LUA_INCLUDE_DIR ${LUA_DIR}/include CACHE PATH "lua include dir" )
set( LUA_LINK_DIR ${LUA_DIR}/lib CACHE PATH "lua link dir" )

find_library( LUA_LIBRARY_DEBUG lua_debug HINTS ${LUA_LINK_DIR} )
if ( NOT LUA_LIBRARY_DEBUG )
    find_library( LUA_LIBRARY_DEBUG lua53_debug HINTS ${LUA_LINK_DIR} )
endif ()

find_library( LUA_LIBRARY_RELEASE lua HINTS ${LUA_LINK_DIR} )
if ( NOT LUA_LIBRARY_RELEASE )
    find_library( LUA_LIBRARY_RELEASE lua53 HINTS ${LUA_LINK_DIR} )
endif ()

if ( CMAKE_BUILD_TYPE STREQUAL "Debug" )
    if ( LUA_LIBRARY_DEBUG )
        set( LUA_LIBRARY ${LUA_LIBRARY_DEBUG} )
    else ()
        set( LUA_LIBRARY ${LUA_LIBRARY_RELEASE} )
    endif ()
else ()
    set( LUA_LIBRARY ${LUA_LIBRARY_RELEASE} )
endif ()

if ( NOT LUA_LIBRARY )
    message( "[Warning] lua library NOT found (LUA_LINK_DIR=${LUA_LINK_DIR})" )
    set( USE_LUA 0 )
else ()
    message( "-- Using lua '${LUA_LIBRARY}'" )
    include_directories( ${LUA_INCLUDE_DIR} )
    set( LUA_LIBRARY "${LUA_LIBRARY}" )
    if ( UNIX )
        set( DEP_LIBS "${DEP_LIBS};dl" )
    endif ()
    set( DEP_LIBS "${DEP_LIBS};${LUA_LIBRARY}" )
    #list( add DEP_LIBS "${LUA_LIBRARY}" )
    set( USE_LUA 1 )
endif ()

