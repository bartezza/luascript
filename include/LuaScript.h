
#ifndef H_LUASCRIPT
#define H_LUASCRIPT

//#define DONT_INCLUDE_LUA

#include <string>
#include <vector>

#ifndef RET_OK
#define RET_OK          (0)
#endif

#ifndef RET_ERROR
#define RET_ERROR       (-1)
#endif

#ifndef DONT_INCLUDE_LUA
extern "C" {
#define LUA_COMPAT_APIINTCASTS
#define LUA_COMPAT_MODULE // all deprecated things!
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
//#include "lautoc.h"
}
#endif

//struct lua_State;

namespace Utils {

    void luaPush(lua_State *L, int v);
    void luaPush(lua_State *L, float v);
    void luaPush(lua_State *L, double v);
    void luaPush(lua_State *L, bool v);
    void luaPush(lua_State *L, const char *v);
    void luaPush(lua_State *L, const std::string &v);

    int luaGet(lua_State *L, int stackPos, int &outV);
    int luaGet(lua_State *L, int stackPos, float &outV);
    int luaGet(lua_State *L, int stackPos, double &outV);
    int luaGet(lua_State *L, int stackPos, bool &outV);
    int luaGet(lua_State *L, int stackPos, std::string &outV);

    /*void luaSet(lua_State *L, const std::string &name, int value);
    void luaSet(lua_State *L, const std::string &name, float value);
    void luaSet(lua_State *L, const std::string &name, double value);
    void luaSet(lua_State *L, const std::string &name, bool value);
    void luaSet(lua_State *L, const std::string &name, const char *value);
    void luaSet(lua_State *L, const std::string &name, const std::string &value);*/

#ifndef DONT_INCLUDE_LUA
    template <typename T>
    void luaSet(lua_State *L, const std::string &name, const T &value) {
        luaPush(L, value);
        lua_setglobal(L, name.c_str());
    }
#endif

    template <typename T>
    void luaSetObj(lua_State *L, const std::string &name, std::vector<T> &v);

    template <typename T>
    int luaGet(lua_State *L, const std::string &name, T &value);

    void luaSetRegistry(lua_State *L, const std::string &name, void *userData);

    void *luaGetRegistry(lua_State *L, const std::string &name);

    template <typename T>
    T luaCheckValue(lua_State *L, int stackPos);

    int luaSetWithUserData(lua_State *L, void *ptr, const char *metatableName, const std::string &name);

    void luaDumpStack(lua_State *L);

    class LuaScript {
    protected:
        lua_State *m_luaState;
        bool m_sandboxed;
        std::string m_lastError;

        //! To read global vars
        int globalIndex(lua_State *L);
        friend int globalIndex2(lua_State *L);

        //! To write global vars
        int globalNewIndex(lua_State *L);
        friend int globalNewIndex2(lua_State *L);

        //! Init the lua state
        void createState();

    public:
        //! Constructor
        //! If sandboxed == true all libs are loaded in the lua state,
        //! otherwise only the "safe" ones
        LuaScript(bool sandboxed = false);

        ~LuaScript();

        //! NOTE: this will leave a lua function on the stack
        int loadFromFile(const std::string &filename);

        //! Load from file and store global function
        int loadFromFile(const std::string &filename, const std::string &funcName);

        int runFromFile(const std::string &filename);

        //! NOTE: this will leave a lua function on the stack
        int loadFromString(const std::string &str);

        //! Load from string and store global function
        int loadFromString(const std::string &str, const std::string &funcName);

        int runFromString(const std::string &str);

        //! Run function already present on the stack
        int run();

        //! Run global function
        int run(const std::string &funcName);

        //! Delete state and recreate a new one
        void reset();
        
        /*template <typename T>
        void setLuaVar(const std::string &name, const T &v);

        template <typename T>
        void getLuaVar(const std::string &name, T &outV) {
            // get global
            lua_getglobal(m_luaState, name.c_str());
            // check
            if (!lua_isnumber(m_luaState, -1)) {
                perr << "Var '" << name << "' is not a number (" << lua_tostring(m_luaState, -1) << ")" << std::endl;
                exit(1);
                //return;
            }
            // to number
            outV = lua_tonumber(m_luaState, -1);
        }*/

        //! Push value on stack
        template <typename T>
        void push(const T &v) {
            luaPush(m_luaState, v);
        }
        /*void push(int v);
        void push(float v);
        void push(double v);
        void push(bool v);
        void push(const char *v);
        void push(const std::string &v);*/

        //! Get value from stack
        template <typename T>
        int get(int stackPos, T &outV) {
            return luaGet(m_luaState, outV);
        }

        //! Push global var on stack
        void pushGlobal(const std::string &name);

        //! Set var
        template <typename T>
        void set(const std::string &name, const T &v) {
            luaSet(m_luaState, name, v);
        }

        template <typename T>
        void setObj(const std::string &name, T &v) {
            luaSetObj(m_luaState, name, v);
        }

        /*void set(const std::string &name, int value);
        void set(const std::string &name, float value);
        void set(const std::string &name, double value);
        void set(const std::string &name, bool value);
        void set(const std::string &name, const char *value);
        void set(const std::string &name, const std::string &value);
        void set(const std::string &name, const std::vector<int> &v);*/

        //! Get global var
        template <typename T>
        int get(const std::string &name, T &value) {
            return luaGet(m_luaState, name, value);
        }

        /*int get(const std::string &name, int &value);
        int get(const std::string &name, float &value);
        int get(const std::string &name, double &value);
        int get(const std::string &name, bool &value);
        int get(const std::string &name, std::string &value);*/

        //! Create a userdata global var, consisting only in a pointer to ptr,
        //! and set its metatable (e.g. used for Setting<Vector3>)
        //int setWithUserData(void *ptr, const char *metatableName, const std::string &name);

        //template <typename T>
        //int setWithUserDataT(T *ptr, const char *metatableName, const std::string &name);

        //! Dump own stack
        void dumpStack() {
            luaDumpStack(m_luaState);
        }

        //! Get state
        inline lua_State *getLua() { return m_luaState; }
        inline const lua_State *getLua() const { return m_luaState; }

        //! Get last error
        inline const std::string &getLastError() const { return m_lastError; }
    };

} // namespace Utils

#endif // H_LUASCRIPT
