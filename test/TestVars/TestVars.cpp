
#include <LuaScript.h>
#include <cstdio>

using namespace Utils;

int main(int argc, char **argv) {
    LuaScript script;
    
    // set a "global" variable
    script.set("glob", "GlobalValue");
    
    // set a global vector (read-write!)
    std::vector<int> vec({5, 2, 9, 4});
    script.setObj("vec", vec);

    // set another global vector
    std::vector<double> vec2(vec.size(), 0.0);
    script.setObj("vec2", vec2);

    // run script
    if (script.runFromString(
            // set a variable in "g.*"
            "g.asd = 'lol'\n"
            // get it back
            "print('asd = ', g.asd)\n"
            // print a global variable
            "print('glob = ', glob)\n"
            // change global variable
            "glob = 'ChangedValue'\n"
            // access to the global vectors
            "for index = 0, 3 do\n"
               // print value
            "  print('vec[' .. index .. '] = ' .. vec[index])\n"
               // set value
            "  vec2[index] = 1.0 / vec[index]\n"
            "end\n"
            // change a value in the vector
            "vec[3] = 123\n"
            // invalid access
            //"vec[4] = 3847\n"
            ) < RET_OK) {
        return 1;
    }

    // get global value
    std::string val;
    int ret = script.get("glob", val);
    if (ret < RET_OK)
        printf("Error getting variable\n");
    printf("Value is '%s'\n", val.c_str());

    // print vector, it should have changed
    printf("Final vec = {");
    for (auto v : vec) {
        printf(" %i", v);
    }
    printf(" }\n");

    // print vector, it should have changed
    printf("Final vec2 = {");
    for (auto v : vec2) {
        printf(" %g", v);
    }
    printf(" }\n");

    return 0;
}
