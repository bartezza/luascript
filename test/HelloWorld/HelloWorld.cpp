
#include <LuaScript.h>
#include <cstdio>

using namespace Utils;

int main(int argc, char **argv) {
        LuaScript script;
    if (script.runFromString(
            "hello = 'Hello world!'"
            "print(hello)"
            ) < RET_OK) {
        return 1;
    }
    return 0;
}
