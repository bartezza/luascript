
#include <LuaScript.h>
#include <cstdio>

using namespace Utils;

int main(int argc, char **argv) {
    // script to run
    const char *str = 
        // do some math
        "a = math.sin(0.123)\n"
        // print stuff
        "print('Hello from script, a = ' .. a)\n";

    // create a non-sandboxed script
    bool sandbox = false;
    LuaScript script(sandbox);
    // run
    script.runFromString(str);
    
    // create a sandboxed script
    sandbox = true;
    LuaScript script2(sandbox);
    // run
    script2.runFromString(str);
    
    printf("Terminated\n");
    return 0;
}

