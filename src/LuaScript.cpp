
#include "LuaScript.h"
#include <cstdio>
#include <cmath>

#include <array>

extern "C" {
#define LUA_COMPAT_APIINTCASTS
#define LUA_COMPAT_MODULE // all deprecated things!
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
//#include "lautoc.h"
}

#define BLACK_STR       "\033[30m"
#define RED_STR         "\033[31m"
#define GREEN_STR       "\033[32m"
#define YELLOW_STR      "\033[33m"
#define BLUE_STR        "\033[34m"
#define MAGENTA_STR     "\033[35m"
#define CYAN_STR        "\033[36m"
#define GRAY_STR        "\033[37m"
#define NOCOLOR_STR     "\033[39m"

#define printError(...) \
      { puts(RED_STR); printf(__VA_ARGS__); puts(NOCOLOR_STR "\n"); }
#define printLog        printf

//#define IFDBGLUA
#define IFDBGLUA    if (0)

namespace Utils {

// prototypes
int globalIndex2(lua_State *L);
int globalNewIndex2(lua_State *L);


// TODO: find a better way to do this!

template <typename T>
struct MetatableTraits { };

template <>
struct MetatableTraits<int> {
    static constexpr const char *name = "int";
};

template <>
struct MetatableTraits<float> {
    static constexpr const char *name = "float";
};

template <>
struct MetatableTraits<double> {
    static constexpr const char *name = "double";
};

template <>
struct MetatableTraits<bool> {
    static constexpr const char *name = "bool";
};

template <>
struct MetatableTraits<std::string> {
    static constexpr const char *name = "string";
};

template <>
struct MetatableTraits<std::vector<int> > {
    typedef std::vector<int> Type;
    typedef int ChildType;
    static constexpr const char *name = "SVector_int";
};

template <>
struct MetatableTraits<std::vector<float> > {
    typedef std::vector<float> Type;
    typedef float ChildType;
    static constexpr const char *name = "SVector_float";
};

template <>
struct MetatableTraits<std::vector<double> > {
    typedef std::vector<double> Type;
    typedef double ChildType;
    static constexpr const char *name = "SVector_double";
};

template <>
struct MetatableTraits<std::vector<bool> > {
    typedef std::vector<bool> Type;
    typedef bool ChildType;
    static constexpr const char *name = "SVector_bool";
};

template <>
struct MetatableTraits<std::vector<std::string> > {
    typedef std::vector<std::string> Type;
    typedef std::string ChildType;
    static constexpr const char *name = "SVector_string";
};

void luaDumpStack(lua_State *L) {
    int n = lua_gettop(L);
    printLog("[Lua] Stack size = %i\n", n);
    for (int i = 1; i <= n; ++i) {
        int type = lua_type(L, i);
        switch (type) {
            case LUA_TNONE:
                printLog("  %i) none\n", i);
                break;
            case LUA_TNIL:
                printLog("  %i) nil\n", i);
                break;
            case LUA_TLIGHTUSERDATA:
                printLog("  %i) light user data\n", i);
                break;
            case LUA_TTABLE:
                printLog("  %i) table\n", i);
                break;
            case LUA_TFUNCTION:
                printLog("  %i) function\n", i);
                break;
            case LUA_TUSERDATA:
                printLog("  %i) user data\n", i);
                break;
            case LUA_TTHREAD:
                printLog("  %i) thread\n", i);
                break;
            case LUA_TSTRING:
                printLog("  %i) string '%s'\n", i, lua_tostring(L, i));
                break;
            case LUA_TBOOLEAN:
                printLog("  %i) boolean '%s'\n", i, lua_toboolean(L, i) ? "true" : "false");
                break;
            case LUA_TNUMBER:
                printLog("  %i) number '%g'\n", i, lua_tonumber(L, i));
                break;
            default:
                printLog("  %i) type '%s'\n", i, lua_typename(L, i));
        }
    }
}

void luaPush(lua_State *L, int v) {
    lua_pushnumber(L, v);
}

void luaPush(lua_State *L, float v) {
    lua_pushnumber(L, v);
}

void luaPush(lua_State *L, double v) {
    lua_pushnumber(L, v);
}

void luaPush(lua_State *L, bool v) {
    lua_pushboolean(L, v);
}

void luaPush(lua_State *L, const char *v) {
    lua_pushstring(L, v);
}

void luaPush(lua_State *L, const std::string &v) {
    lua_pushstring(L, v.c_str());
}

int luaGet(lua_State *L, int stackPos, int &outV) {
    if (lua_isinteger(L, stackPos)) {
        outV = lua_tointeger(L, stackPos);
        return RET_OK;
    } else if (lua_isnumber(L, stackPos)) {
        // get number
        double v = (double) lua_tonumber(L, stackPos);
        // check if it, rounded as int, is equal to itself
        if (rint(v) == v) {
            // yes, treat is as integer
            outV = (int) v;
            return RET_OK;
        } else {
            // nope, it's not an integer
            return RET_ERROR;
        }
    } else {
        return RET_ERROR;
    }
}

int luaGet(lua_State *L, int stackPos, float &outV) {
    if (lua_isnumber(L, stackPos)) {
        outV = (float) lua_tonumber(L, stackPos);
        return RET_OK;
    } else {
        return RET_ERROR;
    }
}

int luaGet(lua_State *L, int stackPos, double &outV) {
    if (lua_isnumber(L, stackPos)) {
        outV = (double) lua_tonumber(L, stackPos);
        return RET_OK;
    } else {
        return RET_ERROR;
    }
}

int luaGet(lua_State *L, int stackPos, bool &outV) {
    if (lua_isboolean(L, stackPos)) {
        outV = lua_toboolean(L, stackPos);
        return RET_OK;
    } else {
        return RET_ERROR;
    }
}

int luaGet(lua_State *L, int stackPos, std::string &outV) {
    if (lua_isstring(L, stackPos)) {
        outV = lua_tostring(L, stackPos);
        return RET_OK;
    } else {
        return RET_ERROR;
    }
}

/*void luaSet(lua_State *L, const std::string &name, int v) {
    lua_pushnumber(L, v);
    lua_setglobal(L, name.c_str());
}

void luaSet(lua_State *L, const std::string &name, float v) {
    lua_pushnumber(L, v);
    lua_setglobal(L, name.c_str());
}

void luaSet(lua_State *L, const std::string &name, double v) {
    lua_pushnumber(L, v);
    lua_setglobal(L, name.c_str());
}

void luaSet(lua_State *L, const std::string &name, bool v) {
    lua_pushboolean(L, v);
    lua_setglobal(L, name.c_str());
}

void luaSet(lua_State *L, const std::string &name, const char *value) {
    lua_pushstring(L, value);
    lua_setglobal(L, name.c_str());
}

void luaSet(lua_State *L, const std::string &name, const std::string &value) {
    lua_pushstring(L, value.c_str());
    lua_setglobal(L, name.c_str());
}*/

/*template <typename T>
void luaSet(lua_State *L, const std::string &name, const T &value) {
    luaPush(L, value);
    lua_setglobal(L, name.c_str());
}*/

template <typename T>
struct LuaPtrUserData {
    T *ptr;
};

template <typename T>
int luaSetWithUserDataT(lua_State *L, T *ptr, const char *metatableName, const std::string &name) {
    //printLog("Pre setWithUserData: "); luaDumpStack(L);

    LuaPtrUserData<T> *ud = (LuaPtrUserData<T> *) lua_newuserdata(L, sizeof(LuaPtrUserData<T>));
    // copy ptr
    ud->ptr = ptr;
    // set metatable
    luaL_getmetatable(L, metatableName);
    lua_setmetatable(L, -2);
    // set global var
    lua_setglobal(L, name.c_str());

    //printLog("Post setWithUserData: "); luaDumpStack(L);

    IFDBGLUA { printLog("%s[Lua] Created userdata named '%s' of type '%s'%s\n", MAGENTA_STR, name.c_str(), metatableName, NOCOLOR_STR); }
    return RET_OK;
}

int luaSetWithUserData(lua_State *L, void *ptr, const char *metatableName, const std::string &name) {
    return luaSetWithUserDataT(L, ptr, metatableName, name);
}

template <typename T>
void luaSetObj(lua_State *L, const std::string &name, std::vector<T> &vect) {
    luaSetWithUserDataT(L, &vect, MetatableTraits<std::vector<T>>::name, name);
}

template void luaSetObj(lua_State *L, const std::string &name, std::vector<int> &vect);
template void luaSetObj(lua_State *L, const std::string &name, std::vector<float> &vect);
template void luaSetObj(lua_State *L, const std::string &name, std::vector<double> &vect);
template void luaSetObj(lua_State *L, const std::string &name, std::vector<bool> &vect);
template void luaSetObj(lua_State *L, const std::string &name, std::vector<std::string> &vect);

template <typename T>
int luaGet(lua_State *L, const std::string &name, T &value) {
    lua_getglobal(L, name.c_str());
    int ret = luaGet(L, -1, value);
    lua_pop(L, 1);
    return ret;
}

template int luaGet(lua_State *L, const std::string &name, int &value);
template int luaGet(lua_State *L, const std::string &name, float &value);
template int luaGet(lua_State *L, const std::string &name, double &value);
template int luaGet(lua_State *L, const std::string &name, bool &value);
template int luaGet(lua_State *L, const std::string &name, std::string &value);

void luaSetRegistry(lua_State *L, const std::string &name, void *userData) {
    lua_pushstring(L, name.c_str());
    lua_pushlightuserdata(L, userData);
    lua_settable(L, LUA_REGISTRYINDEX);
}

void *luaGetRegistry(lua_State *L, const std::string &name) {
    lua_pushstring(L, name.c_str());
    lua_gettable(L, LUA_REGISTRYINDEX);
    void *ptr = lua_touserdata(L, -1);
    lua_pop(L, 1);
    return ptr;
}

/*template <typename T>
T luaCheckValue(lua_State *L, int stackPos);*/

template <>
int luaCheckValue(lua_State *L, int stackPos) {
    return luaL_checkint(L, stackPos);
}

template <>
float luaCheckValue(lua_State *L, int stackPos) {
    return luaL_checknumber(L, stackPos);
}

template <>
double luaCheckValue(lua_State *L, int stackPos) {
    return luaL_checknumber(L, stackPos);
}

template <>
bool luaCheckValue(lua_State *L, int stackPos) {
    return (bool) luaL_checkint(L, stackPos); // TODO: bool??
}

template <>
std::string luaCheckValue(lua_State *L, int stackPos) {
    return luaL_checkstring(L, stackPos);
}

//! Metatable method for handling "vector[index]"
template <typename T>
static int vector_index(lua_State *L) {
    //typedef typename MetatableTraits<T>::ChildType Type;
    // get userdata
    LuaPtrUserData<T> *ud = reinterpret_cast<LuaPtrUserData<T> *>(
        luaL_checkudata(L, 1, MetatableTraits<T>::name)
    );
    // check index
    int index = luaL_checkint(L, 2);
    if (index < 0 || index >= (int) ud->ptr->size())
        return luaL_error(L, "Index overflow reading from %s (index = %d, vector size = %d)", MetatableTraits<T>::name, index, (int) ud->ptr->size());
    // get value from vector
    luaPush(L, (*(ud->ptr))[index]);
    return 1;
}

//! Metatable method for handle "vector[index] = value"
template <typename T>
static int vector_newindex(lua_State *L) {
    typedef typename MetatableTraits<T>::ChildType ChildT;
    // get userdata
    LuaPtrUserData<T> *ud = reinterpret_cast<LuaPtrUserData<T> *>(
        luaL_checkudata(L, 1, MetatableTraits<T>::name)
        );
    // check index
    int index = luaL_checkint(L, 2);
    if (index < 0 || index >= (int) ud->ptr->size())
        return luaL_error(L, "Index overflow writing to %s (index = %d, vector size = %d)", MetatableTraits<T>::name, index, (int) ud->ptr->size());
    // check and get value
    auto value = luaCheckValue<ChildT>(L, 3);
    // set it into vector
    (*(ud->ptr))[index] = value;
    return 0; 
}

template <typename T>
void luaAddVectorType(lua_State *L) {
    // create vector
    luaL_newmetatable(L, MetatableTraits<T>::name);
    // set __index function
    lua_pushcfunction(L, vector_index<T>);
    lua_setfield(L, -2, "__index");
    // set __newindex function
    lua_pushcfunction(L, vector_newindex<T>);
    lua_setfield(L, -2, "__newindex");
    // set __tostring function
    /*lua_pushcfunction(L, vector_toString<std::vector<int>>);
    lua_setfield(L, -2, "__tostring");*/
    // clear stack
    lua_pop(L, 1);
}

/*// create a metatable for our array type
static void create_array_type(lua_State* L) {
   static const struct luaL_reg array[] = {
      { "__index",  array_index  },
      { "__newindex",  array_newindex  },
      NULL, NULL
   };
   luaL_newmetatable(L, "array");
   luaL_openlib(L, NULL, array, 0);
}

// expose an array to lua, by storing it in a userdata with the array metatable
static int expose_array(lua_State* L, int array[]) {
   int** parray = lua_newuserdata(L, sizeof(int**));
   *parray = array;
   luaL_getmetatable(L, "array");
   lua_setmetatable(L, -2);
   return 1;
}*/


// metatable method for handling "vec3[index]"
/*static int vec3_index (lua_State* L) { 
   Vector3 * pvec3 = (Vector3 *) luaL_checkudata(L, 1, "vec3");
   int index = luaL_checknumber(L, 2);
   lua_pushnumber(L, (*pvec3)[index]);
   return 1; 
}

// metatable method for handle "vec3[index] = value"
static int vec3_newindex (lua_State* L) { 
   Vector3 * pvec3 = (Vector3 *) luaL_checkudata(L, 1, "vec3");
   int index = luaL_checknumber(L, 2);
   double value = (double) luaL_checknumber(L, 3);
   (*pvec3)[index] = value;
   return 0; 
}

// create a metatable for our vec3 type
extern "C" {
static void create_vec3_type(lua_State* L) {
   static const luaL_Reg vec3[] = {
      { "__index",  vec3_index  },
      { "__newindex",  vec3_newindex  },
      NULL, NULL
   };
   luaL_newmetatable(L, "vec3");
   luaL_openlib(L, NULL, vec3, 0);
}
}*/

// expose an vec3 to lua, by storing it in a userdata with the vec3 metatable
/*static int expose_vec3(lua_State* L, const std::string &name, Vector3 *vec3) {
   Vector3** pvec3 = (Vector3 **) lua_newuserdata(L, sizeof(Vector3*));
   *pvec3 = vec3;
   luaL_getmetatable(L, name.c_str());
   lua_setmetatable(L, -2);
   return 1;
}*/

LuaScript::LuaScript(bool sandboxed):
    m_luaState(nullptr),
    m_sandboxed(sandboxed)
{
    reset();
}

void LuaScript::reset() {
    // delete existing state
    if (m_luaState) {
        lua_close(m_luaState);
    }
    // create a new one
    createState();
}

void LuaScript::createState() {
    m_luaState = luaL_newstate();
    // sandobex?
    if (!m_sandboxed) {
        // open standard libs
        luaL_openlibs(m_luaState);
    } else {
        // code taken from linit.c of the distribution of lua considered
        // in this case: https://www.lua.org/source/5.3/linit.c.html

        /*
        ** these libs are loaded by lua.c and are readily available to any Lua
        ** program
        */
        static const luaL_Reg toLoad[] = {
            {"_G", luaopen_base},
            //{LUA_LOADLIBNAME, luaopen_package},
            {LUA_COLIBNAME, luaopen_coroutine},
            {LUA_TABLIBNAME, luaopen_table},
            //{LUA_IOLIBNAME, luaopen_io},
            //{LUA_OSLIBNAME, luaopen_os},
            {LUA_STRLIBNAME, luaopen_string},
            {LUA_MATHLIBNAME, luaopen_math},
            {LUA_UTF8LIBNAME, luaopen_utf8},
            {LUA_DBLIBNAME, luaopen_debug},
#if defined(LUA_COMPAT_BITLIB)
            //{LUA_BITLIBNAME, luaopen_bit32},
#endif
            {NULL, NULL}
        };

        /* "require" functions from 'loadedlibs' and set results to global table */
        for (auto *lib = toLoad; lib->func; lib++) {
            luaL_requiref(m_luaState, lib->name, lib->func, 1);
            lua_pop(m_luaState, 1);  /* remove lib */
        }
    }

    // add auto lua
    //luaA_open(m_luaState);
    // expose Vector3
    /*luaA_struct(m_luaState, Vector3);
    luaA_struct_member(m_luaState, Vector3, x, double);
    luaA_struct_member(m_luaState, Vector3, y, double);
    luaA_struct_member(m_luaState, Vector3, z, double);*/

    //printLog("PRE G META: "); dumpStack();

    // create global namespace metatable
    // create metatable
    luaL_newmetatable(m_luaState, "GlobalVars");
    // set __index function
    lua_pushcfunction(m_luaState, globalIndex2);
    lua_setfield(m_luaState, -2, "__index");
    // set __newindex function
    lua_pushcfunction(m_luaState, globalNewIndex2);
    lua_setfield(m_luaState, -2, "__newindex");
    // clear stack from metatable
    lua_pop(m_luaState, 1);
    // create global namespace

    //printLog("POST G META: "); dumpStack();
    
    // create table
    lua_newtable(m_luaState);
    // retrieve globalvars metatable and set it into table
    luaL_getmetatable(m_luaState, "GlobalVars");
    lua_setmetatable(m_luaState, -2);
    // set table as global var
    lua_setglobal(m_luaState, "g");

    //printLog("POST G TABLE: "); dumpStack();

#if 0
    // create vector3 metatable
    luaL_newmetatable(m_luaState, "SVector3");
    // set __index function
    lua_pushcfunction(m_luaState, Vector3_index);
    lua_setfield(m_luaState, -2, "__index");
    // set __newindex function
    lua_pushcfunction(m_luaState, Vector3_newIndex);
    lua_setfield(m_luaState, -2, "__newindex");
    // set __tostring function
    lua_pushcfunction(m_luaState, Vector3_toString);
    lua_setfield(m_luaState, -2, "__tostring");
    // clear stack
    lua_pop(m_luaState, 1);
#endif

    luaAddVectorType<std::vector<int>>(m_luaState);
    luaAddVectorType<std::vector<float>>(m_luaState);
    luaAddVectorType<std::vector<double>>(m_luaState);
    luaAddVectorType<std::vector<bool>>(m_luaState);
    luaAddVectorType<std::vector<std::string>>(m_luaState);

    //printLog("POST VECTOR TYPES: "); dumpStack();
}

LuaScript::~LuaScript() {
    if (m_luaState) {
        lua_close(m_luaState);
    }
}

int LuaScript::loadFromFile(const std::string &filename) {
    // load file
    if (luaL_loadfile(m_luaState, filename.c_str())) {
        m_lastError = lua_tostring(m_luaState, -1);
        lua_pop(m_luaState, 1);
        printError("Error loading '%s': %s", filename.c_str(), m_lastError.c_str());
        return RET_ERROR;
    }
    // NOTE: this will leave a lua function on the stack
    return RET_OK;
}

int LuaScript::loadFromFile(const std::string &str, const std::string &funcName) {
    // load
    if (loadFromFile(str) < RET_OK)
        return RET_ERROR;
    // store function in global
    lua_setglobal(m_luaState, funcName.c_str());
    return RET_OK;
}

int LuaScript::runFromFile(const std::string &filename) {
    // load
    if (loadFromFile(filename) < RET_OK)
        return RET_ERROR;
    // run
    return run();
}

int LuaScript::loadFromString(const std::string &str) {
    if (luaL_loadstring(m_luaState, str.c_str())) {
        m_lastError = lua_tostring(m_luaState, -1);
        lua_pop(m_luaState, 1);
        printError("Error loading from string: %s", m_lastError.c_str());
        return RET_ERROR;
    }
    return RET_OK;
}

int LuaScript::loadFromString(const std::string &str, const std::string &funcName) {
    // load
    if (loadFromString(str) < RET_OK)
        return RET_ERROR;
    // store function in global
    lua_setglobal(m_luaState, funcName.c_str());
    return RET_OK;
}

int LuaScript::runFromString(const std::string &str) {
    // load
    if (loadFromString(str) < RET_OK)
        return RET_ERROR;
    // run
    return run();
}

int LuaScript::run() {
    printLog("PRE RUN: "); dumpStack();
    if (lua_pcall(m_luaState, 0, 0, 0)) {
        m_lastError = lua_tostring(m_luaState, -1);
        lua_pop(m_luaState, 1);
        printError("Error executing script: %s", m_lastError.c_str());
        printLog("POST RUN ERROR: "); dumpStack();
        return RET_ERROR;
    }
    printLog("POST RUN: "); dumpStack();
    return RET_OK;
}

int LuaScript::run(const std::string &funcName) {
    // get global
    lua_getglobal(m_luaState, funcName.c_str());
    // run
    return run();
}

/*template <typename T>
void LuaScript::push(int v) {
    lua_pushnumber(m_luaState, v);
}

void LuaScript::push(float v) {
    lua_pushnumber(m_luaState, v);
}

void LuaScript::push(double v) {
    lua_pushnumber(m_luaState, v);
}

void LuaScript::push(bool v) {
    lua_pushboolean(m_luaState, v);
}

void LuaScript::push(const char *v) {
    lua_pushstring(m_luaState, v);
}

void LuaScript::push(const std::string &v) {
    lua_pushstring(m_luaState, v.c_str());
}*/

void LuaScript::pushGlobal(const std::string &name) {
    //int type =
    lua_getglobal(m_luaState, name.c_str());
    // TODO: check for eventual errors
}

/*void LuaScript::set(const std::string &name, int v) {
    lua_pushnumber(m_luaState, v);
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, float v) {
    lua_pushnumber(m_luaState, v);
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, double v) {
    lua_pushnumber(m_luaState, v);
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, bool v) {
    lua_pushboolean(m_luaState, v);
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, const char *value) {
    lua_pushstring(m_luaState, value);
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, const std::string &value) {
    lua_pushstring(m_luaState, value.c_str());
    lua_setglobal(m_luaState, name.c_str());
}

void LuaScript::set(const std::string &name, const std::vector<int> &vect) {
    setWithUserDataT(&vect, "SVectorInt", name);
}

template <typename T>
int LuaScript::setWithUserDataT(T *ptr, const char *metatableName, const std::string &name) {
    LuaPtrUserData<T> *ud = (LuaPtrUserData<T> *) lua_newuserdata(m_luaState, sizeof(LuaPtrUserData<T>));
    // copy ptr
    ud->ptr = ptr;
    // set metatable
    luaL_getmetatable(m_luaState, metatableName);
    lua_setmetatable(m_luaState, -2);
    // set global var
    lua_setglobal(m_luaState, name.c_str());

    IFDBGLUA { printLog("%s[LuaScript] Created userdata named '%s' of type '%s'%s\n", MAGENTA_STR, name.c_str(), metatableName, NOCOLOR_STR); }
    return RET_OK;
}

int LuaScript::setWithUserData(void *ptr, const char *metatableName, const std::string &name) {
    return setWithUserDataT(ptr, metatableName, name);
}*/



/*template <typename T>
int LuaScript::get(const std::string &name, T &value) {
    pushGlobal(name);
    return get(-1, value);
}

template int LuaScript::get(const std::string &name, int &value);
template int LuaScript::get(const std::string &name, float &value);
template int LuaScript::get(const std::string &name, double &value);
template int LuaScript::get(const std::string &name, bool &value);
template int LuaScript::get(const std::string &name, std::string &value);*/


#if 0 // VECTOR3
template <>
int LuaScript::get<Vector3>(int stackPos, Vector3 &outV) {
    // should be userdata
    if (lua_isuserdata(m_luaState, stackPos)) {
        // get it
        //Vector3 *v = (Vector3 *) luaL_checkudata(m_luaState, stackPos, "Vector3");
        LuaVector3Data *ud = (LuaVector3Data *) luaL_checkudata(m_luaState, stackPos, "SVector3");
        /*if (ud == nullptr) {
            printLog("%s[LuaScript] ERROR: userdata expected%s\n", RED_STR, NOCOLOR_STR);
            return RET_ERROR;
        }*/
        // copy vector
        outV = ud->ptr->value;
    }
    // it could be a table of 3 elements also
    else if (lua_istable(m_luaState, stackPos)) {
        // get table length
        size_t len = lua_rawlen(m_luaState, stackPos);
        if (len != 3) {
            luaL_error(m_luaState, "Expected a table of size 3 for a Vector3 (got %f)", len);
            return RET_ERROR;
        }
        // store table pos in stack
        int tablePos = stackPos;
        // push values on stack
        // first value
        lua_rawgeti(m_luaState, tablePos, 1);
        outV[0] = luaL_checknumber(m_luaState, -1);
        lua_pop(m_luaState, 1);
        // update table pos if necessary
        //if (tablePos < 0) --tablePos;
        // second value
        lua_rawgeti(m_luaState, tablePos, 2);
        outV[1] = luaL_checknumber(m_luaState, -1);
        lua_pop(m_luaState, 1);
        // update table pos if necessary
        //if (tablePos < 0) --tablePos;
        // third value
        lua_rawgeti(m_luaState, tablePos, 3);
        outV[2] = luaL_checknumber(m_luaState, -1);
        // update table pos if necessary
        //if (tablePos < 0) --tablePos;
    }
    else {
        luaL_error(m_luaState, "Invalid type, expected Vector3 (so userdata or table)");
        return RET_ERROR;
    }
    return RET_OK;
}
#endif

int globalIndex2(lua_State *L) {
    //return LuaScript::instance()->globalIndex(L);
    return 0; // TODO
}

#if 0 // TODO
int LuaScript::globalIndex(lua_State *L) {
    IFDBGLUA { printLog("%s[LuaScript] globalIndex()%s\n", MAGENTA_STR, NOCOLOR_STR); }

    // it should point to global var table
    luaL_checktype(L, 1, LUA_TTABLE);

    // last arg should be var name
    luaL_checktype(L, -1, LUA_TSTRING);
    const char *varName = lua_tostring(L, -1);

    // look for setting
    SettingBase *s = getSettingByKey(varName);
    if (s == nullptr) {
        //if (m_errorOnUnknownSetting) {
            luaL_error(L, "Unknown global var '%s'", varName);
        /*} else {
            printLog("%s[LuaScript] Unknown global var '%s', ignoring%s\n", YELLOW_STR, varName, NOCOLOR_STR);
        }*/
        return 0;
    }
    s->push();
    return 1;
}
#endif

int globalNewIndex2(lua_State *L) {
    //return LuaScript::instance()->globalNewIndex(L);
    return 0; // TODO
}

#if 0 // TODO
int LuaScript::globalNewIndex(lua_State *L) {
    IFDBGLUA { printLog("%s[LuaScript] globalNewIndex()%s\n", MAGENTA_STR, NOCOLOR_STR); }

    // it should point to global var table
    luaL_checktype(L, 1, LUA_TTABLE);

    // last arg should be var name
    luaL_checktype(L, -2, LUA_TSTRING);
    const char *varName = lua_tostring(L, -2);

    // look for setting
    SettingBase *s = getSettingByKey(varName);
    if (s == nullptr) {
        if (m_errorOnUnknownSetting) {
            luaL_error(L, "Unknown global var '%s'", varName);
        } else {
            printLog("%s[LuaScript] Unknown global var '%s', ignoring%s\n", YELLOW_STR, varName, NOCOLOR_STR);
        }
        return 0;
    }
    if (s->get(-1) != RET_OK) {
        luaDumpStack(m_luaState);
        exit(1); // EXITING HERE
    }

#if defined(PRINT_SETTING_VALUES) || defined(DEBUG_FLOW_CONTROL)
    parprintLog("[DEBUG] New setting value: '%s' = '%s'\n", varName, s->toString().c_str());
#endif
    return 0;
}
#endif

} // namespace Utils
